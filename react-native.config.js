module.exports = {
    project: {
      ios: {},
      android: {}, // grouped into "project"
    },
    assets: [
      ".android/app/src/main/assets/fonts",
      ".android/app/src/main/assets/colors",
      ".android/app/src/main/assets/images",
    ],
  };