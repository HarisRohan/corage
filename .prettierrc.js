module.exports = {
  arrowParens: 'avoid',
  bracketSameLine: false,
  bracketSpacing: false,
  singleQuote: false,
  trailingComma: 'false',
};

{
    "extends": ["prettier"],
    "plugins": ["prettier"],
    "rules": {
      "prettier/prettier": "error",
      "arrow-body-style": "off",
      "prefer-arrow-callback": "off"
    }
  }