import React, {createContext, useEffect, useState,} from "react";
import AsyncStorage from "@react-native-async-storage/async-storage";
import auth from '@react-native-firebase/auth';

export const AuthContext = createContext();

export const AuthProvider = ({children}) => {
    const [userInfo, setUserInfo] = useState(null);

    const login = async (email, password) => {
        try {
            await auth().signInWithEmailAndPassword(email, password);
        } catch(e) {
            console.log(e);
        }
    };

    const register = async (email, password) => {
        try {
            await auth().createUserWithEmailAndPassword(email, password);
        } catch(e) {
            console.log(e);
        }
    };

    const logout = async () => {
        try {
            await auth().signOut()
        } catch(e) {
            console.log(e);
        }
    };

    // const isLoggedIn = async () => {
    //     try {
    //         setSplashLoading(true);

    //         let userInfo = await AsyncStorage.getItem('userInfo');
    //         userInfo = JSON.parse(userInfo);

    //         if (userInfo) {
    //             setUserInfo(userInfo);
    //         }

    //         setSplashLoading(false);
    //     }
    //     catch (e) {
    //         setIsLoading(false);
    //         console.log(`is logged in error ${e}`);
    //     }
    // };

    // useEffect(() => {
    //     isLoggedIn();
    // }, []);

    return (
        <AuthContext.Provider
            value={{
                userInfo,
                setUserInfo,
                register,
                login,
                logout,
            }}>
            {children}
        </AuthContext.Provider>
    );
};