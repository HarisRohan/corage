import React, { useContext, useState, useEffect } from 'react';
import {NavigationContainer} from '@react-navigation/native';
import auth from '@react-native-firebase/auth';
import { AuthContext } from '../../context/Authentication';
import AuthStack from "../../navigation/AuthStack";
import AppStack from "../AppStack";

const Routes = () => {
    const {userInfo, setUserInfo} = useContext(AuthContext);
    const [initializing, setInitializing] = useState(true);

    const onAuthStateChanged = (userInfo) => {
        setUserInfo(userInfo);
        if (initializing) setInitializing(false);
    }

    useEffect(() => {
        const subscriber = auth().onAuthStateChanged(onAuthStateChanged);
        return subscriber; // unsubscribe on unmount
      }, []);
    
      if (initializing) return null;

    return (
        <NavigationContainer>
            { userInfo ? <AppStack/> : <AuthStack/> }
        </NavigationContainer>
    )
};

export default Routes;