import React, { useContext, useState, useEffect } from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { OnboardingScreen, LoginPage, RegisterPage } from '../../pages';
import AsyncStorage from '@react-native-async-storage/async-storage';

const Stack = createNativeStackNavigator()

const AuthStack = () => {
    const [isFirstLaunch, setIsFirstLaunch] = useState(null);
    let routeName;

    useEffect(() => {
        AsyncStorage.getItem('alreadyLaunched').then(value => {
            if(value == null) {
                AsyncStorage.setItem('alreadyLaunched', 'true');
                setIsFirstLaunch(true);
            } else {
                setIsFirstLaunch(false);
            }
        })
    }, [])

    if (isFirstLaunch == null) {
        return null;
    } else if (isFirstLaunch == true) {
        routeName = 'Onboarding';
    } else {
        routeName = 'Login';
    }

    return (
        <Stack.Navigator initialRouteName={routeName}>
            <Stack.Screen name='Onboarding' component={OnboardingScreen} options={{headerShown:false}} />
            <Stack.Screen name='LoginPage' component={LoginPage} options={{headerShown:false}} />
            <Stack.Screen name='RegisterPage' component={RegisterPage} options={{headerShown:false}} />
            {/* <Stack.Screen name='TermsAndPrivacyPolicy' component={TermsAndPrivacyPolicy} options={{headerShown:false}} /> */}
        </Stack.Navigator>
    )
};

export default AuthStack;