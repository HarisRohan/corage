import React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import {PartOne, HomePage, ProfilePage, Splash, AboutPage, IndividualChat, AddContactPage, EditContactPage, ContactDetail, CallsPage, TermsAndPrivacyPolicy} from '../../pages';
import TabNavigation from '../../components/TabNavigation';
import { createDrawerNavigator } from '@react-navigation/drawer';
import CustomDrawer from '../../components/CustomDrawer';

const Drawer = createDrawerNavigator();
const Stack = createNativeStackNavigator();
const Tab = createBottomTabNavigator();

const DrawerNavigation = () => {
    return(
        <Stack.Navigator>
            <Stack.Screen name='Spalsh' component={Splash} options={{headerShown:false}} />
            <Stack.Screen name='DrawerNavigation' component={DrawerNavigation} options={{headerShown:false}} />
            <Stack.Screen name='HomePage' component={TabNavigation} options={{headerShown:false}} />
            <Stack.Screen name='AddContactPage' component={AddContactPage} options={{headerShown:false}} />
            <Stack.Screen name='EditContactPage' component={EditContactPage} options={{headerShown:false}} />
            <Stack.Screen name='ContactDetail' component={ContactDetail} options={{headerShown:false}} />
            <Stack.Screen name='AboutPage' component={AboutPage} options={{headerShown:false}} />
            <Stack.Screen name='PartOne' component={PartOne} options={{headerShown:false}} />
            <Stack.Screen name='ProfilePage' component={ProfilePage} options={{headerShown:false}} />
            <Stack.Screen name='CallsPage' component={CallsPage} options={{headerShown:false}} />
            <Stack.Screen name='IndividualChat' component={IndividualChat} options={{headerShown:false}} />
        </Stack.Navigator>
    )
}

const AppStack = () => {
    return (
        <Drawer.Navigator drawerContent={props => <CustomDrawer {...props}/>}>
            <Drawer.Screen name='DrawerNavigation' component={DrawerNavigation} options={{headerShown:false}} />
            <Drawer.Screen name='HomePage' component={TabNavigation} options={{headerShown:false}} />
            <Drawer.Screen name='AboutPage' component={AboutPage} options={{headerShown:false}} />
            <Drawer.Screen name='ProfilePage' component={ProfilePage} options={{headerShown:false}} />
            <Drawer.Screen name='TermsAndPrivacyPolicy' component={TermsAndPrivacyPolicy} options={{headerShown:false}} />
        </Drawer.Navigator>
    )
};

export default AppStack;
