import React from 'react';
// import {NavigationContainer} from '@react-navigation/native';
import Routes from './navigation/routes';
import {AuthProvider} from './context/Authentication';

const App = () => {
  return(
      <AuthProvider>
        <Routes/>
      </AuthProvider>
  );
};

export default App;