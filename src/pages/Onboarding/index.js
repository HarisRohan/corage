import React from 'react'
import { View, Text, Image, StyleSheet, Button, TouchableOpacity } from 'react-native'
import Onboarding from 'react-native-onboarding-swiper';
import Colors from '../../../android/app/src/main/assets/colors';

const Dots = ({selected}) => {
    let backgroundColor, width;

    backgroundColor = selected ? Colors.textDarkPrimary : Colors.textDarkSecondary;
    width = selected ? 15 : 5;

    return (
        <View
            style={{
                width,
                height: 5,
                marginHorizontal: 3,
                borderRadius: 10,
                backgroundColor
            }}
        />
    );
}

const Skip = ({ ... props}) => (
    <TouchableOpacity style={{
        width: 173,
        height: 46,
        justifyContent: 'center',
        alignItems: 'center',
        fontFamily: 'Montserrat-Bold',
        fontSize: 16,
        marginBottom: 76,
        marginLeft: 31,
    
    }}
        { ... props }
    >
        <Text style={{
            fontFamily: 'Montserrat-Bold',
            fontSize: 16,
            textTransform: 'capitalize',
            color: Colors.primaryColor,
            
        }}>skip</Text>
    </TouchableOpacity>
);

const Next = ({ ... props}) => (
        <TouchableOpacity style={{
            width: 173,
            height: 46,
            justifyContent: 'center',
            alignItems: 'center',
            fontSize: 16,
            marginBottom: 76,
            backgroundColor: Colors.primaryColor,
            marginRight: 31,
            borderRadius: 47,
            shadowColor: Colors.shadowColor,
            shadowOffset: {
                width: 0,
                height: 1,
            },
            shadowOpacity: 100,
            shadowRadius: 0,
            elevation: 10,
        
        }}
            { ... props }
        >
            <Text style={{
                fontFamily: 'Montserrat-Bold',
                fontSize: 16,
                textTransform: 'capitalize',
                color: 'white',

            }}>Next</Text>
        </TouchableOpacity>
);

const Done = ({ ... props}) => (
    <TouchableOpacity style={{
        width: 173,
        height: 46,
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 76,
        backgroundColor: Colors.primaryColor,
        marginRight: 31,
        borderRadius: 47,
        shadowColor: Colors.shadowColor,
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 100,
        shadowRadius: 0,
        elevation: 10,
    
    }}
        { ... props }
    >
        <Text style={{
            fontFamily: 'Montserrat-Bold',
            fontSize: 16,
            textTransform: 'capitalize',
            color: 'white',

            }}>Start
        </Text>
    </TouchableOpacity>
);

const OnboardingScreen = ({navigation}) => {
  return (
    <Onboarding
        SkipButtonComponent={Skip}
        NextButtonComponent={Next}
        DoneButtonComponent={Done}
        DotComponent={Dots}
        onSkip={() => navigation.replace("LoginPage")}
        onDone={() => navigation.replace("LoginPage")}
        bottomBarColor={Colors.backgroundColor}
        pages={[
            {
                backgroundColor: Colors.backgroundColor,
                image: <Image 
                    source={require('../../../android/app/src/main/assets/images/onboarding1.png')}
                    style={{
                        paddingBottom: -32,
                    }}/>,
                title: 'Manage Contact',
                subtitle: 'Customize your contact list\neasily',
                titleStyles: {
                    color: Colors.textDarkPrimary,
                    fontFamily: 'Montserrat-Bold',
                    marginTop: -20,
                    fontSize: 36,
                    paddingBottom: 31,
                },
                subTitleStyles: {
                    fontFamily: 'IbarraRealNova-SemiBold',
                    fontSize: 18,
                },
            },
            {
                backgroundColor: Colors.backgroundColor,
                image: <Image
                    source={require('../../../android/app/src/main/assets/images/onboarding2.png')}/>,
                title: 'Communicate',
                subtitle: 'Start a conversation with people and share them\nsomething amazing',
                titleStyles: {
                    color: Colors.textDarkPrimary,
                    fontFamily: 'Montserrat-Bold',
                    fontSize: 36,
                    paddingBottom: 31,
                },
                subTitleStyles: {
                    fontFamily: 'IbarraRealNova-SemiBold',
                    fontSize: 18,
                },
            },
            {
                backgroundColor: Colors.backgroundColor,
                image: <Image 
                    source={require('../../../android/app/src/main/assets/images/onboarding3.png')}
                    />,
                title: 'Keep It Simple!',
                subtitle: 'Free tool to keep all your contact\nin one place',
                titleStyles: {
                    color: Colors.textDarkPrimary,
                    fontFamily: 'Montserrat-Bold',
                    fontSize: 36,
                    paddingBottom: 31,
                    marginTop: 9,

                },
                subTitleStyles: {
                    fontFamily: 'IbarraRealNova-SemiBold',
                    fontSize: 18,
                },
            },
        ]}   
    />
  );
};

export default OnboardingScreen;

const styles = StyleSheet.create({
    container: {
        flex : 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
});