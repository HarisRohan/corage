import React, {Component} from 'react';
import {Text, View, TouchableOpacity, StyleSheet, SafeAreaView, Image} from 'react-native';
import Colors from '../../../android/app/src/main/assets/colors';

let thisYear = new Date(). getFullYear();

export class AboutPage extends Component {


    render() {
        return(
            <View style={styles.container}>
                {/* <SafeAreaView> */}
                    <Text style={styles.appName}>CORAGE</Text>
                    <Text style={styles.copyrightText}>Version 1.0</Text>
                    <Image  source={require('../../../android/app/src/main/assets/images/logo.png')}/>
                    <Text style={styles.copyrightText}>{'\u00A9'} {thisYear} BarengSaya. All Rights Reserved</Text>
                {/* </SafeAreaView> */}
            </View>
            
        );
    }
}

export default AboutPage;

const styles = StyleSheet.create({
    container: {
        flex : 1,
        // backgroundColor : 'red',
        // padding: 20,
        alignItems: 'center',
        justifyContent: 'center',
    },
    appName: {
        fontFamily: 'Montserrat-Black',
        color: Colors.textDarkPrimary,
        fontSize: 34,
    },
    copyrightText: {
        fontFamily: 'Montserrat-Regular',
        color: Colors.textDarkPrimary,
        fontSize: 14,
    }
});