import React, {useEffect} from 'react';
import {Image, Text, View, StyleSheet} from 'react-native';
import Colors from '../../../android/app/src/main/assets/colors';

const Splash = ({navigation}) => {
    useEffect(() => {
        setTimeout(() => {
            navigation.replace('HomePage');
        }, 2000);
    });
    return (
        <View style={styles.wrapper}>
        <Image  source={require('../../../android/app/src/main/assets/images/logo.png')}/>
        </View>
    );
};

export default Splash;

const styles = StyleSheet.create({
    wrapper: {
        flex : 1,
        backgroundColor : Colors.backgroundColor,
        alignItems : 'center',
        justifyContent : 'center',
    },
});
