import Splash from './Splash';
import HomePage from './Home';
import PartOne from './PartOne';
import ProfilePage from './Profile';
import AboutPage from './About';
import LoginPage from './Login';
import RegisterPage from './Register';
import OnboardingScreen from './Onboarding';
import AddContactPage from './AddContact';
import EditContactPage from './EditContact';
import TermsAndPrivacyPolicy from './TermsAndPrivacyPolicy';
import ContactDetail from './ContactDetail';
import CallsPage from './Call';
import ChatPage from './Chat';
import IndividualChat from './IndividualChat';

export {Splash, IndividualChat, ChatPage, TermsAndPrivacyPolicy, ContactDetail, HomePage, AddContactPage, EditContactPage, PartOne, ProfilePage, AboutPage, LoginPage, RegisterPage, OnboardingScreen, CallsPage};