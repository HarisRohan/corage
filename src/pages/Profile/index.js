import React, { useContext } from 'react';
import { StyleSheet, View, Text } from 'react-native';
import FormButton from '../../components/Button/buttonForm';
import { AuthContext } from '../../context/Authentication';


const ProfilePage = () => {
    const {userInfo, logout} = useContext(AuthContext);
    return (
        <View style={styles.container}>
            <Text style={styles.text}> Welcome : {userInfo.email}</Text>
            <FormButton buttonTitle='LOGOUT' onPress={() => logout()}/>
        </View>
    );
};

export default ProfilePage;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center',
    },

    text: {
        fontSize: 20,
        color: 'black',
    },

})