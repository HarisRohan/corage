import React, {useContext, useState} from 'react';
import { View, Text, TouchableOpacity, StyleSheet, ScrollView, Image, Alert } from 'react-native';
import firestore from '@react-native-firebase/firestore';
import FormContact from '../../components/Form/formContact.js';
import PrimaryButton from '../../components/Button/buttonPrimary';
import {AuthContext} from '../../context/Authentication';
import Colors from '../../../android/app/src/main/assets/colors';
import Feather from 'react-native-vector-icons/Feather';




const AddContact = ({navigation}) => {
  const {userInfo, logout} = useContext(AuthContext);
  const [name, setName] = useState();
  const [phoneNumber, setPhoneNumber] = useState();
  const [email, setEmail] = useState();
  const [address, setAddress] = useState();

  const createData = async () => {
    console.log('Name : ', name);

    firestore()
    .collection('contact')
    .add({
      userId: userInfo.uid,
      name: name,
      phoneNumber: phoneNumber,
      email: email,
      address: address,
    }).then(() => {
      navigation.replace('HomePage')
      console.log('Contact Saved');
      Alert.alert(
        'Done!',
        'Contact has been saved successfully!',
      );
    }).catch((error) => {
      console.log('Something went wrong', error);
    });
  }

  return (
    <ScrollView contentContainerStyle={styles.container}>
      <TouchableOpacity style={styles.leftArrowContainer} onPress={() => navigation.navigate('HomePage')}>
        <Feather name='arrow-left' size={30} color={Colors.textDarkSecondary}/>
      </TouchableOpacity>
      <View style={styles.formWrapper}>
        <FormContact
            labelValue={name}
            onChangeText={(userName) => setName(userName)}
            placeHolderText='Full Name'
            iconType={'user'}
        />
        <FormContact
            labelValue={phoneNumber}
            onChangeText={(userPhoneNumber) => setPhoneNumber(userPhoneNumber)}
            placeHolderText='Phone Number'
            iconType={'phone'}
            keyboardType="numeric"
            autoCapitalize='none'
            autoCorrect={false}
        />
        <FormContact
            labelValue={email}
            onChangeText={(userEmail) => setEmail(userEmail)}
            placeHolderText='Email'
            iconType={'mail'}
            keyboardType="email-address"
            autoCapitalize='none'
            autoCorrect={false}
        />
        <FormContact
            labelValue={address}
            onChangeText={(userAddress) => setAddress(userAddress)}
            placeHolderText='Address'
            iconType={'map-pin'}
        />
        <View style={styles.buttonContainer}>
          <PrimaryButton
              buttonTitle={'SAVE'}
              onPress={createData}
          />
        </View>
      </View>
    </ScrollView>
  );
};

export default AddContact;

const styles = StyleSheet.create({
  container: {
    // flex: 1,
    justifyContent: 'center',
    marginHorizontal: 35,
    backgroundColor: Colors.backgroundColor,
  },

  formWrapper: {
    marginVertical: 200,
  },
  buttonContainer: {
    flexDirection: 'column',
    alignItems: 'center',
    marginTop: 59,
  },

  leftArrowContainer: {
    position: 'absolute',
    top: 25.85,
    left: 0,
  },
});