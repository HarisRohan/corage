import React, {useContext, useState} from 'react';
import { View, Text, TouchableOpacity, StyleSheet, ScrollView, Image } from 'react-native';
import FormAuthentication from '../../components/Form/formAuthentication';
import FormButton from '../../components/Button/buttonForm';
import SocialButton from '../../components/Button/buttonSocialMediaAuth';
import {AuthContext} from '../../context/Authentication';
import Element1 from '../../../android/app/src/main/assets/images/element1.png';
import Logo from '../../../android/app/src/main/assets/images/logo.png';
import Colors from '../../../android/app/src/main/assets/colors';

const RegisterPage = ({navigation}) => {
  const [name, setName] = useState();
  const [username, setUsername] = useState();
  const [email, setEmail] = useState();
  const [password, setPassword] = useState();

  const {register} = useContext(AuthContext);

  return (
    <ScrollView contentContainerStyle={styles.container}>
        <View style={styles.logoContainer}>
          <Image source={Logo} style={styles.imageLogo}/>
        </View>
        <View style={styles.formWrapper}>
           <FormAuthentication
              labelValue={name}
              onChangeText={(userName) => setName(userName)}
              placeHolderText='Full Name'
              iconType={'person'}
          />
          <FormAuthentication
              labelValue={username}
              onChangeText={(userUsername) => setUsername(userUsername)}
              placeHolderText='Username'
              keyboardType='email-address'
              iconType={'at'}
          />
          <FormAuthentication
              labelValue={email}
              onChangeText={(userEmail) => setEmail(userEmail)}
              placeHolderText='Email'
              iconType={'mail'}
              keyboardType="email-address"
              autoCapitalize='none'
              autoCorrect={false}
          />
          <FormAuthentication
              labelValue={password}
              onChangeText={(userPassword) => setPassword(userPassword)}
              placeHolderText='Password'
              iconType={'lock-closed'}
              secureTextEntry={true}
          />
        </View>

        <View style={styles.signupContainer}>
          <FormButton
              buttonTitle={'SIGN UP'}
              onPress={() => register(email, password)}
          />
        </View>

        <View style={styles.alreadyRegistered}>
          <Text style={styles.OptionText}>Already have an account?</Text>
            <TouchableOpacity onPress={() => (navigation.navigate('LoginPage'))}>
                <Text style={{
                  // marginTop: 5,
                  fontSize: 16,
                  color: Colors.textDarkPrimary,
                  fontFamily: 'Montserrat-Bold',

                }}> LOGIN</Text>
            </TouchableOpacity>
        </View>

        <View style={styles.termPrivacy}>
          <Text style={styles.OptionText}>By continue you agree to our</Text>
          <View style={{flexDirection: 'row'}}>
            <Text style={{
              marginTop: 5,
              fontSize: 16,
              color: Colors.secondaryColor,
              fontFamily: 'Montserrat-Bold',

            }}>Terms </Text>
            <Text style={{
              marginTop: 5,
              fontSize: 16,
              color: Colors.textDarkPrimary,
              fontFamily: 'Montserrat-Medium',

            }}>&</Text>
            <Text style={{
              marginTop: 5,
              fontSize: 16,
              color: Colors.secondaryColor,
              fontFamily: 'Montserrat-Bold',

            }}> Privacy Policy</Text>
          </View>
        </View>        
    </ScrollView>
  );
};

export default RegisterPage;

const styles = StyleSheet.create({
  container: {
    marginHorizontal: 35,
    backgroundColor: Colors.backgroundColor,
  },
  logoContainer: {
    // backgroundColor: 'yellow',
    alignItems: 'center',
  },

  imageLogo: {
    resizeMode: 'contain',
    marginTop: 10,
  },

  formWrapper: {
    // backgroundColor: 'green',
    // marginTop: 10,
  },
  
  signupContainer: {
    // width: '100%',
    // backgroundColor: 'red',
    flexDirection: 'column',
    // justifyContent: 'space-between',
    alignItems: 'center',
    marginTop: 38,
  },
  OptionText: {
    fontSize: 16,
    color: Colors.textDarkPrimary,
    fontFamily: 'Montserrat-Regular',
  },

  alreadyRegistered: {
    flexDirection: 'row',
    marginTop: 38,
    alignItems: 'center',
    justifyContent: 'center',
  },

  termPrivacy: {
    alignItems : 'center',
    paddingTop: 100,
    paddingBottom: 10,
  },
});