import React, {useContext, useEffect, useState} from 'react';
import { View, Text, TouchableOpacity, StyleSheet, ScrollView, Image, Alert } from 'react-native';
import firestore from '@react-native-firebase/firestore';
import FormContact from '../../components/Form/formContact.js';
import PrimaryButton from '../../components/Button/buttonPrimary';
import {AuthContext} from '../../context/Authentication';
import Colors from '../../../android/app/src/main/assets/colors';
import Feather from 'react-native-vector-icons/Feather'




const EditContact = ({id, navigation}) => {
  const {userInfo, logout} = useContext(AuthContext);
  const [name, setName] = useState();
  const [phoneNumber, setPhoneNumber] = useState();
  const [email, setEmail] = useState();
  const [address, setAddress] = useState();
  const [contactData, setContactData] = useState(null);

  const getContactData = async () => {
    await firestore()
    .collection('contact')
    .doc()
    .get()
    .then((documentSnapshot) => {
      if(documentSnapshot.exists) {
        console.log('Contact Data : ', documentSnapshot.data());
        setContactData(documentSnapshot.data())
      }
    }).catch((error) => {
      console.log('Something went wrong', error);
    });
  }

  const updateData = async () => {
    firestore().collection('contact')
    .doc(id)
    .update({
      name: contactData.name,
      phoneNumber: contactData.phoneNumber,
      email: contactData.email,
      address: contactData.address,
      
    }).then(() => {
      console.log('Contact Updated');
      Alert.alert(
        'Done!',
        'Contact has been updated successfully!',
      );
    }).catch((error) => {
      console.log('Something went wrong', error);
    });
  }

  useEffect(() => {
    getContactData();
  }, []);

  return (
    <ScrollView contentContainerStyle={styles.container}>
      <TouchableOpacity style={styles.leftArrowContainer} onPress={() => navigation.navigate('ContactDetail')}>
        <Feather name='arrow-left' size={30} color={Colors.textDarkSecondary}/>
      </TouchableOpacity>
      <TouchableOpacity onPress={Alert.alert('Sorry', "This feature is under development, thanks for evaluating our app")} />
      <View style={styles.formWrapper}>
        <FormContact
            labelValue={{}}
            onChangeText={{}}
            placeHolderText='Full Name'
            iconType={'user'}
        />
        <FormContact
            labelValue={{}}
            onChangeText={{}}
            placeHolderText='Phone Number'
            iconType={'phone'}
            keyboardType="numeric"
            autoCapitalize='none'
            autoCorrect={false}
        />
        <FormContact
            labelValue={{}}
            onChangeText={{}}
            placeHolderText='Email'
            iconType={'mail'}
            keyboardType="email-address"
            autoCapitalize='none'
            autoCorrect={false}
        />
        <FormContact
            labelValue={{}}
            onChangeText={{}}
            placeHolderText='Address'
            iconType={'map-pin'}
        />
        <View style={styles.buttonContainer}>
          <PrimaryButton
              buttonTitle={'UPDATE'}
              onPress={null}
          />
        </View>
      </View>
    </ScrollView>
  );
};

export default EditContact;

const styles = StyleSheet.create({
  container: {
    // flex: 1,
    justifyContent: 'center',
    marginHorizontal: 35,
    backgroundColor: Colors.backgroundColor,
  },

  formWrapper: {
    marginVertical: 200,
  },
  buttonContainer: {
    flexDirection: 'column',
    alignItems: 'center',
    marginTop: 59,
  },

  leftArrowContainer: {
    position: 'absolute',
    top: 25.85,
    left: 0,

  },
});