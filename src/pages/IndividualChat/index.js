import React, {Component, useEffect, useState} from 'react';
import {Text, View, TouchableOpacity, StyleSheet, FlatList, ScrollView, Alert, SafeAreaView} from 'react-native';
import Colors from '../../../android/app/src/main/assets/colors';
import firestore from '@react-native-firebase/firestore';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import MessageForm from '../../components/chatInput/MessageForm';

const IndividualChat = ({navigation, id, route}) => {

//   const [deleted, setDeleted] = useState(false)
//   const [contactData, setContactData] = useState(null);

//   const getContactData = async () => {
//     const contactDocument = [];
//     await firestore()
//     .collection('contact')
//     .doc(route.params.id)
//     .get()
//     .then((documentSnapshot) => {
//       if(documentSnapshot.exists) {
//         console.log('Contact Data : ', documentSnapshot.data());
//         setContactData(documentSnapshot.data())
//       }
//     }).catch((error) => {
//       console.log('Something went wrong', error);
//     });
//   }


//   useEffect( () => {
//     // fetchContact();
//     getContactData();
//   }, [navigation]);



  return(
      <View style={styles.container}>
            <View style={styles.headerWrapper}>
                <View style={styles.leftIndicator}>
                    <TouchableOpacity onPress={Alert.alert('Sorry', 'the stored data is a fake API')} />
                    <TouchableOpacity onPress={() => navigation.navigate('HomePage')}>
                    <SimpleLineIcons name='arrow-left' size={20} color={Colors.textDarkSecondary}/>
                    </TouchableOpacity>
                        <FontAwesome
                            name="user-circle"
                            size={40}
                            color={Colors.textDarkSecondary}
                            style={{
                                marginHorizontal: 15,
                            }}

                        />
                    <Text style={{fontFamily: 'Montserrat-Bold', fontSize: 14, color: Colors.textDarkPrimary, marginTop: 2,}}>Lorem Ipsum</Text>
                </View>
                <View style={{flexDirection: 'row', width: '20%',  alignItems: 'center', justifyContent: 'space-between', }}>
                <TouchableOpacity onPress={() => Alert.alert('Sorry', 'This feature is coming soon, thanks for evaluating our app')}>
                        <SimpleLineIcons name="phone" size={20} color={Colors.textDarkSecondary}/>
                    </TouchableOpacity>
                <TouchableOpacity onPress={() => navigation.navigate('ContactDetail')}>
                    <SimpleLineIcons name='user' size={20} color={Colors.textDarkSecondary}/>
                </TouchableOpacity>
                </View>
            </View>
        <ScrollView showsVerticalScrollIndicator={false}>
            <View style={styles.container}>
                <View style={styles.receivedMessageContainer}>
                    <Text style={styles.receivedMessageText}>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliquaz</Text>
                </View>
                <View style={styles.receivedMessageContainer}>
                    <Text style={styles.receivedMessageText}>Mobile ksdgikdga {'\n'}ywgdyuduyyjdfjkwe{'\n'}fkkgdkfagfkfkasfk</Text>
                </View>
                <View style={styles.sentMessageContainer}>
                    <Text style={styles.sentMessageText}>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</Text>
                </View>
                <View style={styles.sentMessageContainer}>
                    <Text style={styles.sentMessageText}>Mobile ksdgikdga {'\n'}ywgdyuduyyjdfjkwe{'\n'}fkkgdkfagfkfkasfk</Text>
                </View>
                <View style={styles.receivedMessageContainer}>
                    <Text style={styles.receivedMessageText}>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliquaz</Text>
                </View>
                <View style={styles.receivedMessageContainer}>
                    <Text style={styles.receivedMessageText}>Mobile ksdgikdga {'\n'}ywgdyuduyyjdfjkwe{'\n'}fkkgdkfagfkfkasfk</Text>
                </View>
                <View style={styles.sentMessageContainer}>
                    <Text style={styles.sentMessageText}>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</Text>
                </View>
                <View style={styles.sentMessageContainer}>
                    <Text style={styles.sentMessageText}>Mobile ksdgikdga {'\n'}ywgdyuduyyjdfjkwe{'\n'}fkkgdkfagfkfkasfk</Text>
                </View>
                <View style={styles.sentMessageContainer}>
                    <Text style={styles.sentMessageText}>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</Text>
                </View>
                <View style={styles.sentMessageContainer}>
                    <Text style={styles.sentMessageText}>Mobile ksdgikdga {'\n'}ywgdyuduyyjdfjkwe{'\n'}fkkgdkfagfkfkasfk</Text>
                </View>
                <View style={styles.sentMessageContainer}>
                    <Text style={styles.sentMessageText}>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</Text>
                </View>
                <View style={styles.sentMessageContainer}>
                    <Text style={styles.sentMessageText}>Mobile ksdgikdga {'\n'}ywgdyuduyyjdfjkwe{'\n'}fkkgdkfagfkfkasfk</Text>
                </View>
                
            </View>
          </ScrollView>
          <View style={styles.inputTextContainer}>
            <MessageForm/>
          </View>
      </View>
  );
}

export default IndividualChat;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        // backgroundColor: 'yellow',
        // paddingHorizontal: 20,
        paddingHorizontal: 0,
        // alignItems: 'center',
        // justifyContent: 'center',
    },
    headerWrapper: {
        backgroundColor: Colors.backgroundColor,
        // position: 'absolute',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: 20,
        paddingVertical: 10,
        shadowColor: 'grey',
        elevation: 10,

    },
    leftIndicator: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    receivedMessageContainer: {
        backgroundColor: 'white',
        alignSelf: 'flex-start',
        maxWidth: '80%',
        padding: 15,
        marginHorizontal: 20,
        justifyContent: 'center',
        marginTop: 15,
        borderTopRightRadius: 15,
        borderBottomRightRadius: 15,
        borderBottomLeftRadius: 15,
        elevation: 10,
        shadowOpacity: 0.2,
        shadowColor: Colors.textDarkSecondary,
    },
    receivedMessageText: {
        fontFamily: 'Montserrat-Medium',
        fontSize: 14,
        color: Colors.textDarkPrimary,
    },
    sentMessageContainer: {
        backgroundColor: Colors.primaryColor,
        alignSelf: 'flex-end',
        marginHorizontal: 20,
        maxWidth: '80%',
        padding: 15,
        justifyContent: 'center',
        marginTop: 15,
        borderTopLeftRadius: 15,
        borderBottomRightRadius: 15,
        borderBottomLeftRadius: 15,
        elevation: 10,
        shadowOpacity: 0.9,
        shadowColor: Colors.shadowColor,
    },
    sentMessageText: {
        fontFamily: 'Montserrat-Medium',
        fontSize: 14,
        color: 'white',
    },
    inputTextContainer : {
        width: '100%',
        elevation: 10,
        shadowOpacity: 0.9,
        shadowColor: Colors.tertiaryColor,
        backgroundColor: Colors.textDarkQuaternary,
        justifyContent: 'center',
        alignItems: 'center',
        paddingHorizontal: 20,
        position: 'absolute', //Here is the trick
        bottom: 0,

    },
});