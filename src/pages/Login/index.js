import React, {useContext, useState} from 'react';
import { View, Text, TouchableOpacity, StyleSheet, ScrollView, Image } from 'react-native';
import FormAuthentication from '../../components/Form/formAuthentication';
import FormButton from '../../components/Button/buttonForm';
import SocialButton from '../../components/Button/buttonSocialMediaAuth';
import {AuthContext} from '../../context/Authentication';
import Element1 from '../../../android/app/src/main/assets/images/element1.png';
import Logo from '../../../android/app/src/main/assets/images/logo.png';
import Colors from '../../../android/app/src/main/assets/colors';

const LoginPage = ({navigation}) => {
  const [email, setEmail] = useState();
  const [password, setPassword] = useState();

  const {login} = useContext(AuthContext);

  return (
    <ScrollView contentContainerStyle={styles.container}>
        <View style={styles.logoContainer}>
          <Image source={Logo} style={styles.imageLogo}/>
          <Text style={styles.welcomeText}>WELCOME BACK</Text>
        </View>
        <View style={styles.formWrapper}>
          <FormAuthentication
              labelValue={email}
              onChangeText={(userEmail) => setEmail(userEmail)}
              placeHolderText='Email'
              iconType={'mail-outline'}
              keyboardType="email-address"
              autoCapitalize='none'
              autoCorrect={false}
          />
          <FormAuthentication
              labelValue={password}
              onChangeText={(userPassword) => setPassword(userPassword)}
              placeHolderText='Password'
              iconType={'lock-closed-outline'}
              secureTextEntry={true}
          />
        </View>
      

        <View style={styles.loginContainer}>
          <TouchableOpacity>
                <Text style={styles.OptionText}>Forgot Password?</Text>
          </TouchableOpacity>
          <FormButton
              buttonTitle={'LOGIN'}
              onPress={() => login(email, password)}
          />
        </View>

        <View style={styles.bottomStuff}>
          <Text style={styles.OptionText}>Don't have an account?</Text>
            <TouchableOpacity onPress={() => (navigation.navigate('RegisterPage'))}>
                <Text style={{
                  marginTop: 5,
                  fontSize: 16,
                  color: Colors.textDarkPrimary,
                  fontFamily: 'Montserrat-Bold',

                }}>SIGN UP</Text>
            </TouchableOpacity>
            <Image source={Element1} style={styles.imageElement1}/>
        </View>
    </ScrollView>
  );
};

export default LoginPage;

const styles = StyleSheet.create({
  container: {
    marginHorizontal: 35,
    backgroundColor: Colors.backgroundColor,
  },
  logoContainer: {
    // backgroundColor: 'yellow',
    alignItems: 'center',
  },

  imageLogo: {
    resizeMode: 'contain',
    marginTop: 20,
  },

  welcomeText: {
    // marginTop: 38,
    fontSize: 36,
    fontFamily:'Montserrat-Bold',
    textTransform: 'capitalize',
  },

  formWrapper: {
    // backgroundColor: 'green',
    marginTop: 60,
  },
  loginContainer: {
    // width: '100%',
    // backgroundColor: 'red',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginTop: 48,
  },
  OptionText: {
    fontSize: 14,
    color: Colors.textDarkPrimary,
    fontFamily: 'Montserrat-Regular',
  },

  bottomStuff: {
    marginTop: 38,
    alignItems: 'center'
  },

  imageElement1: {
    marginTop: 60,
    resizeMode: 'contain',
  },
});