import React, {Component, useEffect, useState} from 'react';
import {Text, View, TouchableOpacity, StyleSheet, FlatList, ScrollView, Alert} from 'react-native';
import Colors from '../../../android/app/src/main/assets/colors';
import AddButton from '../../components/Button/buttonAdd';
import ContactCard from '../../components/Card/cardContact';
import firestore from '@react-native-firebase/firestore';
import Search from '../../components/Search';

const Contacts = [
    {
        id: '1',
        name: 'Haris Rohan',
        phoneNumber: '082245929046',
        email: 'harisrohanofficial@gmail.com',
        address: 'Omben, Sampang,'
    },
    {
        id: '2',
        name: 'Dani',
        phoneNumber: '085784931713',
        email: 'daniofficial@gmail.com',
        address: 'Omben, Sampang,'
    },
    
]


const HomePage = ({navigation, route}) => {

    const [contactData, setContactData] = useState(null);
    const [deleted, setDeleted] = useState(false);


        const fetchContact = async() => {
            try {
                const list = [];

                await firestore()
                    .collection('contact')
                    .orderBy('name')
                    .get()
                    .then(querySnapshot => {
                        console.log('Total contacts: ', querySnapshot.size);
                        querySnapshot.forEach(doc => {
                            const {userId, name, phoneNumber, email, address} = doc.data();
                            list.push({
                                id: doc.id,
                                userId,
                                name: name,
                                phoneNumber: phoneNumber,
                                email: email,
                                address: address,
                            });
                        });
                    });

                    setContactData(list);
            } catch(error){
                console.log(error);
            }
        }
    
    useEffect( () => {
        fetchContact();
    }, []);

    useEffect( () => {
        fetchContact();
        setDeleted(false)
    }, [deleted]);

    const contactDetail = ({id}) => {
        () => navigation.navigate('ContactDetail', id)
    }

    const deleteContact = async (id) => {
        console.log('Current contact : ', id)

        firestore()
            .collection('contact')
            .doc(id)
            .delete()
            .then(() => {
                Alert.alert(
                    'Contact deleted'
                );
                setDeleted(true)
            })
            .catch((error) => console.log('Error deleting contact', error));
    }

    const updateContact = async (id) => {
        console.log('Current contact : ', id),
        (id) => (navigation.navigate('EditContact', id))
    }

    return(
        <View style={styles.container}>
            <Search    />
            <FlatList
                data={contactData}
                renderItem={({item}) => (<ContactCard
                                            item={item}
                                            onDelete={deleteContact}
                                            onPress={() => navigation.navigate('ContactDetail', {userId: item.userId})}
                                        />
                                    
                                        )}
                keyExtractor={item => item.id}
                showsVerticalScrollIndicator={false}
            />
            <View style={styles.buttonContainer}>
                <AddButton
                    buttonTitle={'+'}
                    onPress={() => (navigation.navigate('AddContactPage'))}
                />
            </View>
        </View>
    );
}

export default HomePage;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.backgroundColor,
        marginHorizontal: 20,
    },
    buttonContainer: {
        // backgroundColor: Colors.backgroundColor,
        position: 'absolute',
        bottom: 120,
        right: 0,
    },
});