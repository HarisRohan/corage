import React, {Component, useEffect, useState} from 'react';
import {Text, View, TouchableOpacity, StyleSheet, FlatList, ScrollView, Alert, SafeAreaView} from 'react-native';
import Colors from '../../../android/app/src/main/assets/colors';
import firestore from '@react-native-firebase/firestore';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';



const ContactDetail = ({navigation, id, route}) => {

  const [contactData, setContactData] = useState(null);

  // useEffect( () => {
  //   const fetchContact = async() => {
  //     try {
  //         // const list = [];

  //         const storeContact = await firestore()
  //             .collection('contact')
  //             .doc(contactId)
  //             .where('id', '==', route.params.id)
  //             .get()
  //             .then(querySnapshot => {
  //                 console.log('Total contacts: ', querySnapshot.size);
  //                 querySnapshot.forEach(doc => {
  //                     const {userId, name, phoneNumber, email, address} = doc.data();
  //                     push({
  //                         id: doc.id,
  //                         userId,
  //                         name: name,
  //                         phoneNumber: phoneNumber,
  //                         email: email,
  //                         address: address,
  //                     });
  //                 });
  //             });

  //             setContactData(list);
  //     } catch(error){
  //         console.log(error);
  //     }
  //   }
  //   fetchContact();
  // }, [contactId]);

  const getContactData = async () => {
    const contactDocument = [];
    await firestore()
    .collection('contact')
    .doc(route.params.id)
    .get()
    .then((documentSnapshot) => {
      if(documentSnapshot.exists) {
        console.log('Contact Data : ', documentSnapshot.data());
        setContactData(documentSnapshot.data())
      }
    }).catch((error) => {
      console.log('Something went wrong', error);
    });
  }


  useEffect( () => {
    // fetchContact();
    getContactData();
  }, [navigation]);


  return(
      <SafeAreaView>
        <View style={styles.container}>
          <View style={styles.headerWrapper}>
            <TouchableOpacity onPress={() => navigation.navigate('HomePage')}>
              <SimpleLineIcons name='arrow-left' size={20} color={Colors.textDarkSecondary}/>
            </TouchableOpacity>
            <View style={{flexDirection: 'row', width: '20%',  alignItems: 'center', justifyContent: 'space-between'}}>
              <TouchableOpacity onPress={() => navigation.navigate('EditContactPage')}>
                <SimpleLineIcons name='pencil' size={20} color={Colors.textDarkSecondary}/>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => Alert.alert('Sorry', 'This feature is coming soon, thanks for evaluating our app')}>
                <SimpleLineIcons name='trash' size={20} color={Colors.textDarkSecondary}/>
              </TouchableOpacity>
            </View>
          </View>
          <View style={styles.sectionOneWrapper}>
              <TouchableOpacity style={styles.avatarIconWrapper} onPress={Alert.alert('Sorry', 'The stored data is fake API')}>
                 <FontAwesome name="user-circle" size={80} color={Colors.textDarkSecondary}/>
              </TouchableOpacity>
              <Text style={{fontFamily: 'Montserrat-Black', fontSize: 40, color: Colors.textDarkPrimary}}>LOREM IPSUM</Text>
              <View style={styles.actionIconWrapper}>
                <TouchableOpacity onPress={() => navigation.navigate('IndividualChat')}>
                 <SimpleLineIcons name="bubbles" size={20} color={Colors.textDarkSecondary}/>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => Alert.alert('Sorry', 'This feature is coming soon, thanks for evaluating our app')}>
                  <SimpleLineIcons name="phone" size={20} color={Colors.textDarkSecondary}/>
                </TouchableOpacity>
              </View>
              <Text style={{fontFamily: 'Montserrat-Regular', fontSize: 14, color: Colors.textDarkPrimary, marginTop: 2,}}>"Your bio goes here"</Text>
          </View>
          <View style={styles.sectionTwoWrapper}>
            <View style={{marginTop: 55}}>
              <Text style={{fontFamily: 'Montserrat-Regular', fontSize: 14, color: Colors.textDarkSecondary}}>Mobile</Text>
              <Text style={{fontFamily: 'Montserrat-Regular', fontSize: 20, color: Colors.textDarkPrimary}}>+01 2345 6789 10</Text>
            </View>
            <View style={{marginTop: 55}}>
              <Text style={{fontFamily: 'Montserrat-Regular', fontSize: 14, color: Colors.textDarkSecondary}}>Email</Text>
              <Text style={{fontFamily: 'Montserrat-Regular', fontSize: 20, color: Colors.textDarkPrimary}}>johndoe@example.com</Text>
            </View>
            <View style={{marginTop: 55}}>
              <Text style={{fontFamily: 'Montserrat-Regular', fontSize: 14, color: Colors.textDarkSecondary}}>Address</Text>
              <Text style={{fontFamily: 'Montserrat-Regular', fontSize: 20, color: Colors.textDarkPrimary}}>Houston, Texas</Text>
            </View>
          </View>
          </View>
      </SafeAreaView>
  );
}

export default ContactDetail;

const styles = StyleSheet.create({
    container: {
        // flex: 1,
        backgroundColor: Colors.backgroundColor,
        marginHorizontal: 20,
    },
    headerWrapper: {
        // backgroundColor: 'red',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginTop: 20,
    },
    sectionOneWrapper: {
      justifyContent: 'center',
      alignItems: 'center',
    },
    avatarIconWrapper: {
      backgroundColor: Colors.textDarkTertiary,
      justifyContent: 'center',
      alignItems: 'center',
      width: 170,
      height: 170,
      borderRadius: 150,
      marginVertical: 15,
    },
    actionIconWrapper: {
      marginVertical: 24,
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center', 
      width: '30%',
    },
    sectionTwoWrapper: {
      paddingHorizontal: 45,
      backgroundColor: 'white',
      width: 389,
      height: 414,
      justifyContent: 'center',
      alignItems: 'flex-start',
      marginTop: 115,
      borderTopLeftRadius: 28,
      borderTopRightRadius: 28,
      elevation: 10,
      shadowOpacity: 0.2,
      shadowColor: Colors.textDarkSecondary,
    },
});