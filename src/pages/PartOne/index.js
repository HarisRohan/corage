import React, { Component } from "react";
import {View, StyleSheet, TextInput, TouchableOpacity, Text, Alert} from "react-native";

class PartOne extends Component {
  constructor(props) {
    super(props);
    this.state = {
      firstName: null,
      lastName: null,
      fullName: null,
    };
  }

  getFirstName = (text) => {
    this.setState({ firstName: text });
  };

  getLastName = (text) => {
    this.setState({ lastName: text });
  };

  Combine = () => {
    let firstInput = this.state.firstName;
    let secondInput = this.state.lastName;
    if (
      firstInput !== null &&
      secondInput !== null &&
      firstInput !== " " &&
      secondInput !== " "
    ) {
      this.setState({ fullName: firstInput + " " + secondInput });
    } else {
      Alert.alert("Warning", "Please, input your name");
    }
  };

  render() {
    return (
      <View style={styles.wrapper}>
        <TextInput
          style={styles.form}
          placeholder="First Name"
          onChangeText={(text) => this.getFirstName(text)}
        />
        <TextInput
          style={styles.form}
          placeholder="Last Name"
          onChangeText={(text) => this.getLastName(text)}
        />
        <TouchableOpacity
          style={styles.button}
          // title="Combine"
          onPress={() => this.Combine()}
        >
          <Text
            style={{
              color: "white",
              textAlign: "center",
              position: "absolute",
            }}
          >
            CHECK
          </Text>
        </TouchableOpacity>
        <Text style={styles.revealForm}>{this.state.fullName}</Text>
      </View>
    );
  }
}

export default PartOne;

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    padding: 20,
    flexDirection: "column",
    backgroundColor: "#FFF8D1",
    alignItems: "baseline",
    justifyContent: "center",
    position: "relative",
  },
  form: {
    borderBottomColor: "#ded7b1",
    height: 40,
    width: "100%",
    textAlign: "left",
    margin: 3,
    paddingLeft: 20,
    borderBottomWidth: 2,
  },
  revealForm: {
    textTransform: "uppercase",
    textAlign: "center",
    fontSize: 20,
    fontWeight: "bold",
    borderBottomColor: "black",
    height: 40,
    width: "100%",
    // borderBottomWidth: 1,
  },
  button: {
    backgroundColor: "#ffb300",
    borderRadius: 100,
    height: 40,
    width: "100%",
    alignItems: "center",
    marginTop: 25,
    marginBottom: 50,
    justifyContent: "center",
    paddingTop: 20,
  },
});
