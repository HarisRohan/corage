import React from 'react';
import { View, TextInput, StyleSheet } from 'react-native';
import { windowHeight, windowWidth } from '../utils/Dimentions';
import Fontisto from 'react-native-vector-icons/Fontisto'; 
import Ionicons from 'react-native-vector-icons/Ionicons';
import Colors from '../../../android/app/src/main/assets/colors';

const FormAuthentication = ({
  labelValue,
  placeHolderText,
  iconType,
  ...rest
}) => {
  return (
    <View style={styles.formContainer}>
      <View style={styles.iconStyle}>
        <Ionicons name={iconType} size={30} color="white"/>
      </View>
      <TextInput
        label={labelValue}
        placeholder={placeHolderText}
        style={styles.InputText}
        {...rest}
      />
    </View>
  );
};

export default FormAuthentication;

const styles = StyleSheet.create({
    formContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: Colors.textDarkQuaternary,
        borderRadius: 10,
        marginTop: 10,
        // paddingVertical: 10,
    },

    iconStyle: {
        backgroundColor: Colors.primaryColor,
        justifyContent: 'center',
        alignItems: 'center',
        width: 70,
        height: 70,
        borderBottomLeftRadius: 10,
        borderTopLeftRadius: 10,
        
    },

    InputText: {
        marginLeft: 12,
        color: '#262626',
        fontFamily: 'Montserrat-Regular',
        fontSize: 16,
        width: '100%'
    }
});