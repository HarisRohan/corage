import React from 'react';
import { View, TextInput, StyleSheet } from 'react-native';
import { windowHeight, windowWidth } from '../utils/Dimentions';
import Fontisto from 'react-native-vector-icons/Fontisto'; 
import Feather from 'react-native-vector-icons/Feather';
import Colors from '../../../android/app/src/main/assets/colors';

const FormContact = ({
  labelValue,
  placeHolderText,
  iconType,
  ...rest
}) => {
  return (
    <View style={styles.formContainer}>
      <View style={styles.iconStyle}>
        <Feather name={iconType} size={24} color={Colors.textDarkSecondary}/>
      </View>
      <TextInput
        label={labelValue}
        placeholder={placeHolderText}
        style={styles.InputText}
        {...rest}
      />
    </View>
  );
};

export default FormContact;

const styles = StyleSheet.create({
    // formContainer: {
    //     flexDirection: 'row',
    //     alignItems: 'center',
    //     backgroundColor: Colors.backgroundColor,
    //     marginTop: 16,
    //     borderBottomColor: Colors.textDarkSecondary,
    //     borderBottomWidth: 1,
    // },

    formContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: Colors.textDarkQuaternary,
        borderRadius: 10,
        marginTop: 10,
        padding: 10,
        paddingLeft: 20,
    },

    iconStyle: {
        // backgroundColor: Colors.primaryColor,
        justifyContent: 'center',
        alignItems: 'center',
        // width: 50,
        // height: 50,
        borderBottomLeftRadius: 8,
        borderTopLeftRadius: 8,
    },

    InputText: {
        marginLeft: 15,
        color: '#262626',
        fontFamily: 'Montserrat-Regular',
        fontSize: 14,
        width: '100%'
    }
});