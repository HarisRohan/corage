import React from 'react';
import { View, Text, TouchableOpacity, StyleSheet } from 'react-native';
import { windowHeight, windowWidth } from '../../utils/Dimentions';

import FontAwesome from 'react-native-vector-icons/FontAwesome';

const SocialMediaButton = ({ buttonTitle, buttonType, color, backgroundColor, ...rest}) => {
    let BGColor = backgroundColor;
    return (
        <TouchableOpacity style={[styles.buttonContainer, {backgroundColor: BGColor}]} {...rest}>
            <View style={styles.iconWrapper}>
                <FontAwesome name={buttonType} style={styles.icon} size={22} color={color}/>
            </View>
            <View style={styles.ButtonTextWrapper}>
                <Text style={[styles.buttonText, {color: color}]}>{buttonTitle}</Text>
            </View>
        </TouchableOpacity>
  )
}

export default SocialMediaButton;

const styles = StyleSheet.create({
    buttonContainer: {
        marginTop: 10,
        width: '100%',
        height: windowHeight / 15,
        padding: 10,
        flexDirection: 'row',
        borderRadius: 3,
    },

    iconWrapper: {
        width: 30,
        justifyContent: 'center',
        alignItems: 'center',
    },

    icon: {
        fontWeight: 'bold',
    },

    ButtonTextWrapper: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },

    buttonText: {
        fontSize: 18,
        fontFamily: 'Montserrat-Bold',
    },

});