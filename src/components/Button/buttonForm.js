import React from 'react';
import { Text, TouchableOpacity, StyleSheet, View } from 'react-native';
import Colors from '../../../android/app/src/main/assets/colors';
import { windowHeight, windowWidth } from '../../utils/Dimentions';

const ButtonForm = ({buttonTitle, ...rest}) => {
  return (
    <TouchableOpacity
      style={styles.buttonContainer}
      {...rest}
    >
        <Text style={styles.buttonTitle}>{buttonTitle}</Text>
    </TouchableOpacity>
  )
}

export default ButtonForm;

const styles = StyleSheet.create({
    buttonContainer: {
        width: 143,
        height: 46,
        backgroundColor: Colors.primaryColor,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 8,
        shadowColor: Colors.shadowColor,
        shadowOffset: {
            width: 3,
            height: -1,
        },
        shadowOpacity: 0.4,
        shadowRadius: 0,
        elevation: 5,
        // marginRight: 3,
    },

    buttonTitle: {
        color: 'white',
        fontFamily: 'Montserrat-Medium',
        fontSize: 18,
    },
})