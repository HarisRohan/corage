import React from 'react';
import { Text, TouchableOpacity, StyleSheet, View } from 'react-native';
import Colors from '../../../android/app/src/main/assets/colors';
import { windowHeight, windowWidth } from '../../utils/Dimentions';

const buttonPrimary = ({buttonTitle, ...rest}) => {
  return (
    <TouchableOpacity
      style={styles.buttonContainer}
      {...rest}
    >
        <Text style={styles.buttonTitle}>{buttonTitle}</Text>
    </TouchableOpacity>
  )
}

export default buttonPrimary;

const styles = StyleSheet.create({
    buttonContainer: {
        width: 300,
        height: 50,
        backgroundColor: Colors.textDarkPrimary,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 50,
        shadowColor: Colors.textDarkPrimary,
        shadowOffset: {
            width: 3,
            height: 3,
        },
        shadowOpacity: 0.4,
        // shadowRadius: 0,
        elevation: 10,
        // marginRight: 3,
    },

    buttonTitle: {
        color: Colors.backgroundColor,
        fontFamily: 'Montserrat-Bold',
        fontSize: 32,
    },
})