import React, { useContext } from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { DrawerContentScrollView, DrawerItemList } from '@react-navigation/drawer';
import { AuthContext } from '../../context/Authentication';
import Colors from '../../../android/app/src/main/assets/colors';
import Ionicons from 'react-native-vector-icons/Ionicons';


const CustomDrawer = (props) => {
    const {userInfo, logout} = useContext(AuthContext);
  return (
    <View style={styles.container}>
        <View style={styles.avatarContainer}>
                <Ionicons name="ios-person-circle-outline" size={80} color={Colors.textDarkSecondary}/>
                <View style={styles.userInformation}>
                <Text style={{fontFamily: 'Montserrat-Bold', fontSize: 16, color: Colors.textDarkPrimary}}>{userInfo.email}</Text>
                {/* <Text style={{fontFamily: 'Montserrat-Regular', fontSize: 12, color: Colors.textDarkPrimary}}>{userInfo.email}</Text> */}
                </View>
        </View>
        <DrawerContentScrollView>
            <View style={styles.drawerListContainer}>
                <TouchableOpacity style={styles.drawerList} onPress={() => props.navigation.navigate('ProfilePage')}>
                    <Ionicons name='ios-person-outline' size={26}/>
                    <Text style={styles.listText}>Profile</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.drawerList} onPress={() => props.navigation.navigate('AboutPage')}>
                    <Ionicons name='md-information' size={26}/>
                    <Text style={styles.listText}>App Info</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.drawerList} onPress={() => props.navigation.navigate('TermsAndPrivacyPolicy')}>
                <Ionicons name='shield-outline' size={26}/>
                    <Text style={[styles.listText]}>Terms & Privacy Policy</Text>
                </TouchableOpacity>
            </View>
        </DrawerContentScrollView>
        <View style={styles.logoutOption}>
            <TouchableOpacity style={{flexDirection: 'row', alignItems: 'center',}} onPress={() => logout()}>
                <Ionicons name='ios-power-outline' size={26}/>
                <Text style={styles.listText}>Sign Out</Text>
            </TouchableOpacity>
        </View>
    </View>
  )
}

export default CustomDrawer;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.backgroundColor,
    },
    avatarContainer : {
        paddingVertical: 35,
        paddingLeft: 20,
        borderBottomColor: Colors.textDarkTertiary,
        borderBottomWidth: 1,
    },
    userInformation: {
        paddingTop: 5,
    },
    drawerListContainer: {
        paddingLeft: 20,
    },
    drawerList: {
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 20
    },
    listText: {
        fontFamily: 'Montserrat-Medium',
        fontSize: 16,
        paddingLeft: 20,
    },
    logoutOption: {
        paddingLeft: 20,
        paddingVertical: 25,
        borderTopColor: Colors.textDarkTertiary,
        borderTopWidth: 1,
        justifyContent: 'center',
        // alignItems: 'center'
    },
})