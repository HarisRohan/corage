import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import {PartOne, ChatPage, HomePage, ProfilePage, Splash, AboutPage, AddContactPage, EditContactPage, ContactDetail, CallsPage} from '../../pages';
import Colors from '../../../android/app/src/main/assets/colors';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';

const BottomTab = createBottomTabNavigator();

const TabNavigation = () => {
    return (
        <BottomTab.Navigator
            screenOptions={{
                headerStyle: {
                    backgroundColor: Colors.backgroundColor,
                    elevation: 0,
                },
                headerTintColor: Colors.primaryColor,
                headerTitleStyle: {
                    fontFamily: 'Montserrat-Bold',
                    fontSize: 48,
                    position: 'absolute',
                    // bottom: 0,
                },
                tabBarActiveTintColor: Colors.primaryColor,
                tabBarInactiveTintColor: Colors.textDarkTertiary,
                tabBarShowLabel: false,
                // headerShown: false,
                // headerTransparent: true,
                tabBarStyle: {
                    backgroundColor: 'white',
                    position: 'absolute',
                    bottom: 25,
                    height: 76,
                    right: 35,
                    left: 35,
                    borderRadius: 15,
                    shadowColor: Colors.textDarkTertiary,
                    shadowOpacity: 0.5,
                    elevation: 3,
                                        
                }
            }}
        >
            <BottomTab.Screen name='Contacts' component={HomePage} options={{
                    tabBarIcon: ({color, size}) => (
                        <Ionicons
                            name="people-outline"
                            color={color}
                            size={size}
                        />
                    )
                }}
            />

            <BottomTab.Screen name='Chat' component={ChatPage} options={{
                    tabBarIcon: ({color, size}) => (
                        <Ionicons
                            name="chatbubbles-outline"
                            color={color}
                            size={size}
                        />
                    )
                }}
            />
            <BottomTab.Screen name='Calls' component={CallsPage}options={{
                    tabBarIcon: ({color, size}) => (
                        <SimpleLineIcons
                            name="phone"
                            color={color}
                            size={size}
                        />
                    )
                }}
            />
            
        </BottomTab.Navigator>
    );
}

export default TabNavigation;

// const styles = StyleSheet.create{(

// )}

