import React from 'react';
import { View, Text, StyleSheet, TextInput } from 'react-native';
import Colors from '../../../android/app/src/main/assets/colors';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';

const Search = () => {
  return (
    <View style={styles.formContainer}>
      <SimpleLineIcons name='magnifier' size={15} color={Colors.textDarkSecondary}/>
      <TextInput
        placeholder={'Search   '}
        style={styles.InputText}
      />
    </View>
  )
}

export default Search;

const styles = StyleSheet.create({
  formContainer: {
    height: 38,
    width: '100%',
    backgroundColor: Colors.textDarkTertiary,
    // justifyContent: 'center',
    borderRadius: 50,
    marginVertical: 12,
    paddingHorizontal: 13,
    flexDirection: 'row',
    alignItems: 'center',
  }, 
  InputText: {
    paddingHorizontal: 12,
    color: Colors.textDarkQuaternary,
    fontFamily: 'Montserrat-Regular',
    fontSize: 16,



},
  userContainer: {
    backgroundColor: 'red',
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 8.85,
  },

  titleSearchContainer: {
    backgroundColor: 'green',
    marginTop: 9,
  },
});