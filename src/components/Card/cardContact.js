import React, { useEffect, useState } from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import Colors from '../../../android/app/src/main/assets/colors';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import firestore from '@react-native-firebase/firestore';


const ContactCard = ({item, onPress}) => {

  return (

    <TouchableOpacity style={styles.container} onPress={onPress}>
        <FontAwesome name='user-circle' size={40}/>
        <Text style={styles.contactName}>{item.name}</Text>
    </TouchableOpacity>
  )
}

export default ContactCard;

const styles = StyleSheet.create({
    container: {
        backgroundColor: Colors.backgroundColor,
        // width: '100%',
        height: 70,
        justifyContent: 'flex-start',
        alignItems: 'center',
        flexDirection: 'row',
        borderBottomWidth: 2,
        borderBottomColor: Colors.textDarkTertiary,
        // fontFamily: 'Montserrrat-Bold',
        // fontSize: 12,
        paddingHorizontal: 20,
    },

    contactName: {
        fontFamily: 'Montserrat-Bold',
        fontSize: 20,
        marginHorizontal: 10,
    }
})