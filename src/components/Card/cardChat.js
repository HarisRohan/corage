import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import Colors from '../../../android/app/src/main/assets/colors';
import FontAwesome from 'react-native-vector-icons/FontAwesome';


const ChatCard = ({item, onPress}) => {
  return (
    <TouchableOpacity style={styles.container} onPress={onPress}>
        <View style={styles.chatInformation}>
            <View>
                <FontAwesome name='user-circle' size={50} style={{paddingRight: 10,}}/>
                <View style={styles.isOnline}/>
            </View>
            <View >
                <Text style={styles.username}>{item.name}</Text>
                <Text style={styles.latestChat}>Lorem ipsum dolor sit amet{'\n'}consectetur adipiscing elit, sed do eiusmod </Text>
            </View>
        </View>
        <View style={styles.markWrapper}>
            <View style={styles.unreadMark}>
                <Text style={styles.unreadChatNumbers}>4</Text>
            </View>
            <Text style={styles.timeIndicator}>18.45</Text>
        </View>
    </TouchableOpacity>
  )
}

export default ChatCard;

const styles = StyleSheet.create({
    container: {
        width: '100%',
        height: 70,
        justifyContent: 'space-between',
        alignItems: 'center',
        flexDirection: 'row',
    },
    chatInformation: {
        flexDirection: 'row'
    },
    isOnline: {
        width: 15,
        height: 15,
        backgroundColor: Colors.tertiaryColor,
        borderRadius: 50,
        borderColor: 'white',
        borderWidth: 2,
        position: 'absolute',
        alignSelf: 'baseline',
        bottom: -1,
        right: 15,
    },
    username: {
        fontFamily: 'Montserrat-Bold',
        fontSize: 20,
    },
    latestChat: {
        fontFamily: 'Montserrat-Regular',
        fontSize: 11,
    },
    markWrapper: {
        height: '100%',
        alignItems: 'center',
        justifyContent: 'space-evenly'
    },
    unreadMark: {
        width: 15,
        height: 15,
        backgroundColor: Colors.primaryColor,
        borderRadius: 50,
        alignItems: 'center',
        justifyContent: 'center',
        shadowColor: Colors.primaryColor,
        elevation: 1,
    },
    unreadChatNumbers: {
        fontFamily: 'Montserrat-Regular',
        fontSize: 8,
        color: 'white'
    },
    timeIndicator: {
        fontFamily: 'Montserrat-Regular',
        fontSize: 11,
    }
})