import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import Colors from '../../../android/app/src/main/assets/colors';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Feather from 'react-native-vector-icons/Feather';


const CallCard = ({item}) => {
  return (
    <TouchableOpacity style={styles.container}>
        <View style={styles.chatInformation}>
            <View>
                <FontAwesome 
                    name='user-circle'
                    size={50}
                    style={{paddingRight: 10,}}
                />
                {/* <View style={styles.isOnline}/> */}
            </View>
            <View >
                <Text style={styles.username}>{item.name}</Text>
                <View style={{flexDirection: 'row'}}>
                    <Text style={styles.timeIndicator}>18.45</Text>
                    <Feather 
                        name='arrow-down-left'
                        size={12}
                        style={{paddingLeft: 10, color: Colors.tertiaryColor}}
                    />
                    <Feather 
                        name='arrow-up-right'
                        size={12}
                        style={{color: 'red'}}
                    />
                </View>
            </View>
        </View>
        {/* <View style={styles.markWrapper}>
            <View style={styles.unreadMark}>
                <Text style={styles.unreadChatNumbers}>4</Text>
            </View>
            <Text style={styles.timeIndicator}>18.45</Text>
        </View> */}
    </TouchableOpacity>
  )
}

export default CallCard;

const styles = StyleSheet.create({
    container: {
        width: '100%',
        height: 70,
        justifyContent: 'space-between',
        alignItems: 'center',
        flexDirection: 'row',
    },
    chatInformation: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    username: {
        fontFamily: 'Montserrat-Bold',
        fontSize: 20,
    },
    timeIndicator: {
        fontFamily: 'Montserrat-Regular',
        fontSize: 11,
    },
})