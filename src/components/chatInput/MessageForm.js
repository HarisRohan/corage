import React from 'react';
import { StyleSheet, Text, TextInput, TouchableOpacity, View, Alert } from 'react-native';
import Colors from '../../../android/app/src/main/assets/colors';
import Ionicons from 'react-native-vector-icons/Ionicons';


const MessageForm = () => {
  return (
    <View style={styles.container}>
        <TouchableOpacity onPress={() => Alert.alert('Sorry', 'This feature is coming soon, thanks for evaluating our app')}>
            <Ionicons name='happy' size={30}/>
        </TouchableOpacity>
        <TextInput 
            placeholder='Type your message'
            style={styles.inpuText}
        />
        <TouchableOpacity onPress={() => Alert.alert('Sorry', 'This feature is coming soon, thanks for evaluating our app')}>
            <Ionicons name='mic' size={30}/>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => Alert.alert('Sorry', 'This feature is coming soon, thanks for evaluating our app')}>
            <Ionicons name='ios-attach-outline' size={30}/>
        </TouchableOpacity>        
    </View>
  )
}

export default MessageForm;

const styles = StyleSheet.create({
    container: {
        backgroundColor: Colors.textDarkQuaternary,
        width: '100%',
        height: 70,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',

    },
    inpuText: {
        paddingHorizontal: 12,
        color: Colors.textDarkPrimary,
        fontFamily: 'Montserrat-Regular',
        fontSize: 16,
        width: '60%',
        maxWidth: '80%',
    }
})