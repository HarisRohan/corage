import styled, { StyledComponent } from "styled-components";
import Colors from '../../../android/app/src/main/assets/colors';

export const Container = styled.view`
    flex: 1;
    align-items: center;
    justify-content: left;
`