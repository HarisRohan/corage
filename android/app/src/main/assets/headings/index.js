const Headings = {
    h1 : {
        fontFamily : 'Montserrat-Bold',
        fontSize : 34,
    }
}

export default Headings;