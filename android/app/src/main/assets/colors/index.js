const Colors = {
    backgroundColor : '#F3F3F3',
    primaryColor : '#34ABEB',
    secondaryColor : '#DA6007',
    tertiaryColor : '#29E6A6',
    textDarkPrimary : '#616161',
    textDarkSecondary : '#949494',
    textDarkTertiary : '#E5E5E5',
    textDarkQuaternary : '#eaeaea',
    shadowColor : '#0289A1',
}

export default Colors;